#include "Table.h"
#include "math.h"
#include <algorithm>
#include <stdio.h>
#include <cstring>

using namespace std;
Table::Table(int64_t table_size) : size(table_size)
{
    current_queen = 0;
    Rows.resize(table_size);
    Cols.resize(table_size);
    for(register int i = 0; i < table_size; ++i)
    {
        Rows[i] = false;
        Cols[i] = false;
    }
    BuildDiagonals();
}

Table::~Table()
{
    //dtor

}

void Table::AddDiagonal(const int64_t &position)
{
    int64_t x,y,tx,ty;
    ConvertToDimensions(x,y,position);
    occupiedDiagonal.AddDiagonal(position);

    tx = x-1;
    ty = y-1;
    while(tx>=0 || ty>=0)
    {
        occupiedDiagonal.AddDiagonal(ConvertFromDimensions(tx,ty));
        tx -= 1;
        ty -= 1;
    }

    tx = x-1;
    ty = y+1;
    while(tx>0 || ty<size)
    {
        occupiedDiagonal.AddDiagonal(ConvertFromDimensions(tx,ty));
        tx -= 1;
        ty += 1;
    }

    tx = x+1;
    ty = y+1;
    while(ty<size || tx<size)
    {
        occupiedDiagonal.AddDiagonal(ConvertFromDimensions(tx,ty));
        tx += 1;
        ty += 1;
    }

    tx = x+1;
    ty = y-1;
    while(ty>0 || tx<size)
    {
        occupiedDiagonal.AddDiagonal(ConvertFromDimensions(tx,ty));
        tx += 1;
        ty -= 1;
    }

}


void Table::BuildDiagonals()
{
    for(register int x = 0; x < size; ++x)
    {
        DiagonalTest d;
        for(register int y = 0; y < size; ++y)
        {
            if(x+y>size)
                break;
            d.AddDiagonal(ConvertFromDimensions(x+y,x+y));
        }
        Diagonals.push_back(d);
    }

    for(register int x = 0; x < size; ++x)
    {
        DiagonalTest d;
        for(register int y = size; y >=0 ; --y)
        {
            if(y-x<0 || x+y>size)
                break;
            d.AddDiagonal(ConvertFromDimensions(x+y,y-x));
        }
        Diagonals.push_back(d);
    }
}

void Table::MarkDiagonal(const int64_t &pos)
{

    for(DiagonalTest *d = &Diagonals[0]; d ; ++d)
    {
        if(d->TestDiagonal(pos))
        {
            d->isFree = false;
        }
    }
}


bool Table::AddQueen(int64_t position)
{
    int64_t x,y;
    ConvertToDimensions(x,y,position);
    if(QueenCount()==0)
    {
        queens.push_back(position);
        Rows[x] = true;
        Cols[y] = true;
        AddDiagonal(position);

        return true;
    }

    if(Rows[x]&&Cols[y])
        return false;

    std::vector<int64_t>::iterator loc = std::lower_bound(queens.begin(), queens.end(), position);
    if(*loc!=position)
    {
        queens.insert(loc,position);
        Rows[x] = true;
        Cols[y] = true;
        AddDiagonal(position);
        return true;
    }

    return false;
}
int64_t Table::AddQueen(int64_t X, int64_t Y)
{
    return AddQueen(ConvertFromDimensions(X, Y));
}
bool Table::TestQueens()
{
    if(QueenCount()==0)
    {
        std::cout << "No Queens placed!" << std::endl;
        return false;
    }
    int64_t q;
    current_queen = 0;
    for(register int i = 0; i < queens.size(); ++i)
    {
        if(i==current_queen)
            continue;
        q = TestQueensById(0);
        if(q>=0)
        {
            std::cout << "Queen " << current_queen << " hits on Queen "  << i << std::endl;
            return true;
        }

        ++current_queen;
    }
    std::cout << "Queens doesn't hit each other!" << std::endl;
    return false;

}
int64_t Table::TestQueensById(int queen_id)
{
    int64_t q;
    for(int64_t i = 0; i < size; ++i)
    {
        q = TestQueensByLevel(queens[queen_id],i);
        if(q>=0)
            return q;

    }
    return -1;
}
void Table::GetQueenLocation(int64_t &X, int64_t &Y, unsigned int queen_id)
{
    if(queen_id > QueenCount())
        return;
    ConvertToDimensions(X,Y,queens[queen_id]);
}
int64_t Table::TestQueensByPosition(const int64_t &X, const int64_t &Y)
{
    return TestQueensByLevel(ConvertFromDimensions(X,Y),0);
}
int64_t Table::TestQueensPosition(int64_t queen_position)
{
    if(queen_position<0)
        return -1;
    if(binary_search(queens.begin(), queens.end(), queen_position))
    {
        std::vector<int64_t>::iterator loc = std::lower_bound(queens.begin(), queens.end(), queen_position);
        return *loc;
    }
    return -1;
}

int64_t Table::ConvertFromDimensions(const int64_t &X, const int64_t &Y)
{
    int64_t fpos = size*Y + X;
    return fpos <= (size*size) ? fpos : -1;
}
void Table::ConvertToDimensions(int64_t &X, int64_t &Y, int64_t pos)
{
    X = pos%size;
    Y = floor(pos/size);
}

int64_t Table::OperationPosition(const int64_t &X, const int64_t &Y, int64_t pos)
{
    int64_t op = pos + (X + (Y*size));
    return (op < 0 || op >= size*size) ? -1 : op;
}

bool Table::InvalidDimension(int64_t* m)
{
    return (m[0]<0 || m[1]<0 || m[0]>=size || m[1] >= size);
}

bool Table::IsOccupied(const int64_t &X, const int64_t &Y)
{
    int64_t d = ConvertFromDimensions(X,Y);
    return (TestQueensPosition(d)>0);
}

int64_t Table::TestQueensByLevel(int64_t queen_position, int64_t level)
{
    int64_t x,y,d;
    ConvertToDimensions(x,y,queen_position);
    if(Rows[x]||Cols[y])
        return queen_position;

    int64_t m[4][2];
    m[UP_LEFT][0] = x-level; m[UP_LEFT][1] = y+level;
    m[DOWN_LEFT][0] = x-level; m[DOWN_LEFT][1] = y-level;
    m[DOWN_RIGHT][0] = x+level; m[DOWN_RIGHT][1] = y-level;
    m[UP_RIGHT][0] = x+level; m[UP_RIGHT][1] = y+level;

    if(InvalidDimension(m[UP_LEFT])&& InvalidDimension(m[DOWN_LEFT])&& InvalidDimension(m[DOWN_RIGHT])&& InvalidDimension(m[UP_RIGHT]) )
        return -1;
    for(int i = 0; i < 4; ++i)
    {
        if(InvalidDimension(m[i]))
            continue;
        //if(SimpleTest(m[i][0],m[i][1])==HIT)
        d = ConvertFromDimensions(m[i][0],m[i][1]);
        if(TestQueensPosition(d)>0)
            return d;
    }
    return TestQueensByLevel(queen_position, level+1);
}

CHECK_STATUS Table::SimpleTest(const int64_t &pos)
{
    int64_t x,y;
    ConvertToDimensions(x,y,pos);
    return SimpleTest(x,y);
}
CHECK_STATUS Table::SimpleTest(const int64_t &x, const int64_t &y)
{
    if(x<0 || y<0 || x>=size || y >= size)
        return INVALID;
    return(Rows[x]||Cols[y]) ? HIT : OK;
}

bool Table::ExecuteQueenPlacement()
{
    //DefaultPlacing();
    HorsePlacing(size);
}

int void1(int square)
{
    if(square==0)
    {

    }
}

bool Table::HorsePlacing(int num_queens)
{
    if(QueenCount()==0)
        AddQueen(0,0);
   int64_t x = 0, y = 0;
   cout<<"Add queen at " << x <<":"<<y<<endl;
   do
   {
      x += 1;
      y += 2;
      if(y>=size)
        y = ColSweep();
      if(AddQueen(x,y))
        cout<<"Add queen at " << x <<":"<<y<<endl;
      else
        cout<<"QUEEN ERROR AT " << x <<":"<<y<<endl;
   }while(QueenCount() < num_queens);

    return true;

}

bool Table::DefaultPlacing()
{

    for(register int i = 0; i < size; ++i)
    {
        int x = RowSweep();
        int y = ColSweep();
        if(!FindAndPlaceQueen(x,y))
            return false;
    }
    return true;
}


bool Table::FindAndPlaceQueen(int x, int y)
{
    if(x < 0 || x >= size)
    {
        return FindAndPlaceQueen(RowSweep(x), ColSweep(y+1));
    }
    if(y < 0 || y >= size)
    {
        return FindAndPlaceQueen(RowSweep(x+1), ColSweep(y));
    }

    if((x < 0 && y < 0) || (x >= size && y>=size))
    {
        return false;
    }

    do
    {
        if(y<0)
        {
          y = 0;
           break;
        }
        std::cout << "Testing tile " << x << ":" << y << std::endl;
        if(SimpleTest(x,y)==OK  && !DiagonalSweep(ConvertFromDimensions(x,y)))
        {

            if(AddQueen(x,y))
            {
                std::cout << " - Adding queen at " << x << ":" << y << std::endl;
                return true;
            }

        }
        y = ColSweep(y+1);
    }while(y < size);

    return FindAndPlaceQueen(RowSweep(x+1), ColSweep(y));
}

int Table::RowSweep(int offset)
{
    int c = offset;
    for(register int i = 0; i < size;++i, ++c)
    {
        if(c>=size)
            c = 0;
       if(!Rows[c])
            return c;

    }
    return -1;
}

int Table::ColSweep(int offset)
{
    int c = offset;
    for(register int i = 0; i < size;++i, ++c)
    {
        if(c>=size)
            c = 0;
       if(!Cols[c])
            return c;
    }
    return -1;
}



bool Table::DiagonalSweep(const int64_t &position)
{
    return occupiedDiagonal.TestDiagonal(position);
}

void Table::PrintTable()
{
    printf("===============TABLE===========\n");
    for(register int y = 0; y < size; ++y)
    {
        for(register int x = 0; x < size; ++x)
        {
            if(!IsOccupied(x,y))
                printf("O ");
            else
                printf("X ");
        }
        printf("\n");
    }
}
