#pragma once
#include "DiagonalTest.h"

DiagonalTest::DiagonalTest() : isFree(true)
{
    //ctor
}

DiagonalTest::~DiagonalTest()
{
    //dtor
}

bool DiagonalTest::TestDiagonal(const int64_t &position)
{
    if(position<0)
        return false;
    if(binary_search(tiles.begin(), tiles.end(), position))
    {
        return true;
    }
    return false;
}

void DiagonalTest::AddDiagonal(const int64_t &position)
{
    if(tiles.size()==0)
    {
       tiles.push_back(position);
       return;
    }

    std::vector<int64_t>::iterator loc = std::lower_bound(tiles.begin(), tiles.end(), position);
    if(*loc!=position)
    {
        tiles.insert(loc,position);
    }
}
