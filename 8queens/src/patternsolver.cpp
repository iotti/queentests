#include "patternsolver.h"

PatternSolver::PatternSolver(Table *t) : BaseSolver(t)
{
    patterns[0].push_back(PatternPosition({0,1}));
    patterns[0].push_back(PatternPosition({1,3}));
    patterns[0].push_back(PatternPosition({2,5}));
    patterns[0].push_back(PatternPosition({3,0}));
    patterns[0].push_back(PatternPosition({4,2}));
    patterns[0].push_back(PatternPosition({5,4}));

    patterns[1].push_back(PatternPosition({0,2}));
    patterns[1].push_back(PatternPosition({1,5}));
    patterns[1].push_back(PatternPosition({2,1}));
    patterns[1].push_back(PatternPosition({3,4}));
    patterns[1].push_back(PatternPosition({4,0}));
    patterns[1].push_back(PatternPosition({5,3}));
}

int PatternSolver::Solve(unsigned short nqueens)
{
    char idx = 0;
    bool patternType = false;

    unsigned short currentX = 0, currentY = 0;
    PatternPosition *cp;
    std::vector<PatternPosition>&currentPattern = patterns[(int)patternType];

    for(register int i = 0; i < nqueens; ++i, ++idx)
    {
        cp = &currentPattern[idx];
        if(!table->AddQueen(cp->x + currentX, cp->y + currentY ))
        {
            printf("POSICAO INVALIDA!\n");
            return -1;
        }
        if(idx >= currentPattern.size()-1)
        {
           patternType = !patternType;
           currentX += currentPattern.size()-1;
           currentY += currentPattern.size()-1;
           idx = 0;
           currentPattern = patterns[(int)patternType];
        }

    }
    return 0;

}
