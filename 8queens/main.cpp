#pragma once
#include <iostream>
#include <Table.h>
#include <time.h>
#include <patternsolver.h>
clock_t tStart;
unsigned int n_queens;
int64_t n_tiles;
clock_t time_begin(unsigned int number_of_queens, int64_t table_size)
{
    n_queens = number_of_queens;
    n_tiles = table_size;
    tStart = clock();
}
static void time_end(){printf("Time taken for %i queens on %lld x %lld table size was %.4fs\n", n_queens, n_tiles, n_tiles, (double)(clock() - tStart)/CLOCKS_PER_SEC);}


using namespace std;


bool execute_test_1(int table_size, int placed_queens)
{
    Table t(table_size);
    int64_t lx = 0,ly = 0;
    std::cout << "Placing " << placed_queens << " queens..."<< std::endl;
    for(register int i = 0; i < placed_queens;++i)
    {

        int64_t rx = rand()%table_size, ry = rand()%table_size, add = -1;

        int ret_count = 0;

        if(lx!=0 && ry != 0)
        {
            do
            {
                if(ret_count == 5)
                {
                    for(int j = 0; j < t.Getsize();++j)
                    {
                        if(t.IsRowFree(j))
                        {
                            rx = j;
                            break;
                        }
                    }
                    ry = 0;
                }
                else if(ret_count > 5)
                {
                    //rx += 2;
                    ry += 1;
                    if(rx>=t.Getsize())
                    {
                        rx -= t.Getsize()-1;
                    }
                    if(ry>=t.Getsize())
                    {
                        ry -= t.Getsize();
                    }
                }
                else
                {
                    rx = rand()%table_size;
                    ry = rand()%table_size;
                }
                std::cout << "Trying place queen at " << rx << " x " << ry << std::endl;
                add = t.TestQueensByPosition(rx,ry);
                printf("%i\n",add);
                ++ret_count;


            }while(add >= 0);
        }

        t.AddQueen(rx,ry);
        lx = rx;
        ly = ry;

        std::cout << "Placing queen at " << rx << " x " << ry << std::endl;

    }

    std::cout << "Queens placed... Adding remaining ones..." << std::endl;
    //t.AddQueen(0,0);
    //t.AddQueen(1,1);
    //t.AddQueen(4,2);
    /*int64_t x = 0, y = 0,add = -1;
    for(register int i = 0; i < t.Getsize()-placed_queens; ++i)
    {
        int64_t queen = rand()%t.QueenCount();
        t.GetQueenLocation(x,y,queen);
        std::cout << "Testing from queen " << queen << " at " << x << " x " << y << std::endl;
        add = -1;
        do
        {
            x += 2;
            y += 1;
            if(x>=t.Getsize())
            {
                x -= t.Getsize()-1;
            }
            if(y>=t.Getsize())
            {
                y -= t.Getsize();
            }
            add = t.TestQueensByPosition(x,y);
        }while(add >= 0);
         std::cout << "Adding queen at " << x << " x " << y << std::endl;
        t.AddQueen(add);
    }*/
    time_begin(t.QueenCount(), t.Getsize());
    bool result = t.TestQueens();
    time_end();
    return result;
}

void Pattern6Execution()
{
    Table t(8);
    time_begin(t.QueenCount(), t.Getsize());
    PatternSolver solver(&t);
    if(solver.Solve(t.Getsize()) >= 0)
    {
        printf("SUCESSFUL!\n");
        t.TestQueens();
        time_end();
        t.PrintTable();
    }
    else
    {
        printf("FAILURE!\n");
        time_end();
    }

}

void EmptyExecution()
{
    Table t(8);
    time_begin(t.QueenCount(), t.Getsize());
    bool result = t.ExecuteQueenPlacement();
    t.TestQueens();
    time_end();
}


int main(int argc, char *argv[])
{
    srand(time( new time_t()));
    //execute_test_1(8,7);
    //EmptyExecution();
    Pattern6Execution();
    system("pause");
    return 0;
}

