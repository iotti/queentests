#ifndef BASESOLVER_H
#define BASESOLVER_H
#include <Table.h>

class BaseSolver
{
public:
    BaseSolver(Table*t);
    Table *table;

    virtual int Solve(unsigned short nqueens) = 0;
};

#endif // BASESOLVER_H
