#ifndef TABLE_H
#define TABLE_H
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <algorithm>
#include <iostream>
#include <iterator>
#include <vector>
#include <list>
#include <DiagonalTest.h>

struct QueenMoves
{
    int64_t targets[8];
};

enum DIRECTIONS
{
    UP_LEFT = 0,
    UP_RIGHT,
    DOWN_LEFT,
    DOWN_RIGHT,
    LEFT,
    DOWN,
    RIGHT,
    UP
};

enum CHECK_STATUS
{
    INVALID = -1,
    OK,
    HIT
};




class Table
{
    public:
        Table(int64_t table_size);
        virtual ~Table();
        int64_t Getsize() { return size; }
        unsigned int QueenCount() { return queens.size(); }


        bool AddQueen(int64_t position);
        int64_t AddQueen(int64_t X, int64_t Y);

        bool TestQueens();

        bool MoveQueen(unsigned int queen_id,int64_t X, int64_t Y);

        int64_t TestQueensByPosition(const int64_t &X, const int64_t &Y);


        int64_t ConvertFromDimensions(const int64_t &X, const int64_t &Y);
        void ConvertToDimensions(int64_t &X, int64_t &Y, int64_t pos);

        void GetQueenLocation(int64_t &X, int64_t &Y, unsigned int queen_id);

        CHECK_STATUS SimpleTest(const int64_t &pos);
        CHECK_STATUS SimpleTest(const int64_t &X, const int64_t &Y);


        bool IsRowFree(int64_t x){return !Rows[x];};
        bool IsColFree(int64_t y){return !Cols[y];};

        bool ExecuteQueenPlacement();

        void PrintTable();



    protected:
    private:
        int64_t size;
        std::vector<int64_t> queens;
        unsigned int current_queen;

        std::vector<bool> Rows,Cols;
        std::vector<DiagonalTest> Diagonals;
        DiagonalTest occupiedDiagonal;

        void MarkDiagonal(const int64_t &pos);
        //bool Rows[1024],Cols[1024];

        //Test Queeen By Id
        int64_t TestQueensById(int queen_id);

        //Tests if there is a queen in that position
        int64_t TestQueensPosition(int64_t queen_position);

        //Tests the locations where the queen can go
        int64_t TestQueensByLevel(int64_t queen_position, int64_t level);
        int64_t OperationPosition(const int64_t &X, const int64_t &Y, int64_t pos);

        bool InvalidDimension(int64_t* m);

        bool HorsePlacing(int num_queens = -1);

        bool DefaultPlacing();

        bool IsOccupied(const int64_t &X, const int64_t &Y);

        int ColSweep(int offset = 0);
        int RowSweep(int offset = 0);

        bool DiagonalSweep(const int64_t &position);

        void BuildDiagonals();

        void AddDiagonal(const int64_t &position);

        bool FindAndPlaceQueen(int x, int y);
};

#endif // TABLE_H
