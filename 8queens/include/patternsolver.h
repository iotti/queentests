#ifndef PATTERNSOLVER_H
#define PATTERNSOLVER_H
#include <basesolver.h>

struct PatternPosition{
    unsigned short x;
    unsigned short y;

    Pattern(unsigned short a[]){ x = a[0]; y = a[1];}
};

struct Pattern{
    PatternPosition pos[6];
};



class PatternSolver : public BaseSolver
{
public:
    PatternSolver(Table *t);
    virtual int Solve(unsigned short nqueens);



private:

    std::vector<PatternPosition> patterns[2];
};

#endif // PATTERNSOLVER_H
