#ifndef DIAGONALTEST_H
#define DIAGONALTEST_H
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <algorithm>
#include <iostream>
#include <iterator>
#include <vector>
#include <list>


class DiagonalTest
{
    public:
        DiagonalTest();
        virtual ~DiagonalTest();



        bool TestDiagonal(const int64_t &position);

        void AddDiagonal(const int64_t &position);

        bool isFree;
    protected:
    private:
        std::vector<int64_t> tiles;

};

#endif // DIAGONALTEST_H
